// main.js

import { createApp } from 'vue'
import router from './router'
import store from './store'
import App from './App.vue'

store.commit("setIsLoading", true)
setTimeout(() => {
    store.commit("setIsLoading", false)
}, 1000);

createApp(App).use(router).use(store).mount('#app')
