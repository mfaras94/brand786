// router/index.js

import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import LoginView from '@/views/LoginView.vue'
import RegisterView from '@/views/RegisterView.vue'
import CreateToDoView from '@/views/CreateToDoView.vue'
import ToDoListView from '@/views/ToDoListView.vue'
import ViewUpdateToDoView from '@/views/ViewUpdateToDoView.vue'
import NotFoundView from '@/views/NotFoundView.vue'
import store from '@/store'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomeView,
    meta: {
      requiresAuth: false,
      redirectIfLoggedIn: true
    }

  },
  {
    path: '/login',
    name: 'Login',
    component: LoginView,
    meta: {
      requiresAuth: false,
      redirectIfLoggedIn: true
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: RegisterView,
    meta: {
      requiresAuth: false,
      redirectIfLoggedIn: true 
    }
    
  },
  {
    path: '/create',
    name: 'CreateToDo',
    component: CreateToDoView,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/todo',
    name: 'ToDoList',
    component: ToDoListView,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/update',
    name: 'ViewUpdateToDo',
    component: ViewUpdateToDoView,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:notFound',
    name: 'NotFound',
    component: NotFoundView
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const redirectIfLoggedIn = to.matched.some(route => route.meta.redirectIfLoggedIn);
  const isLoggedIn = store.getters.isLoggedIn

  if (requiresAuth) {
    if (isLoggedIn) {
      next()
    } else {
      next({ name: 'Home' })
    }
  } else if(redirectIfLoggedIn && isLoggedIn) {
    next({name: 'ToDoList'})
  }
  else{
    next()
  }
})

export default router
