// store/index.js

import { createStore } from 'vuex'
import auth from './modules/auth'
import state from './state'
import mutations from './mutations'
import getters from './getters'
import actions from './actions'


const store = createStore({
  modules: {
    auth,
  },  
    state,
    mutations,
    getters,
    actions,
})

store.dispatch('authentication');

export default store
