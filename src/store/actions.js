
import axios from "axios";
import router from "@/router";
export default {
  // Action for Get Todo Items
  async getTodoItems({ commit }) {
    commit("setIsLoading", true);
    try {
      const token = sessionStorage.getItem("authToken");
      const response = await axios.get("http://3.232.244.22/api/items", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.data.success) {
        commit("setTodoItems", response.data.items.data);
  
      }
      commit("setIsLoading", false);
    } catch (error) {
      console.log(error);
    } finally {
      commit("setIsLoading", false);
    }
  },

  //   Action for create New Todo

  async createTodo({ commit }, { title, description }) {
    commit("setIsLoading", true);
    try {
      const token = sessionStorage.getItem("authToken");
      const response = await axios.post("http://3.232.244.22/api/item", {
        title,
        description,
      },{
        headers: {
            Authorization: `Bearer ${token}`,
          },
      });

      if (response.data.success) {
        router.push({name: "ToDoList"})
      }
      commit("setIsLoading", false);
    } catch (error) {
      console.log(error);
    } finally {
      commit("setIsLoading", false);
    }
  },

  async deleteTodo({commit , dispatch}, itemId){
    try {
      const token = sessionStorage.getItem("authToken");
      const response = await axios.delete(`http://3.232.244.22/api/item/${itemId}`,{
        headers:{
          Authorization: `Bearer ${token}`,
        },
      })
      if (response.data.success) {
          alert(response.data.message)
          commit("deleteTodoItem",itemId)
          dispatch("getTodoItems")
    
      }
      
    } catch (error) {
      console.log(error);
    }
    
  }
};
