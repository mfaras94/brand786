export default{
    setTodoItems(state, value) {
        state.todoItems = value
      },
      setSearchTerm(state, term) {
        state.searchTerm = term;
      },
      deleteTodoItem(state, value){
        state.deleteTodoItem = value
      }
}