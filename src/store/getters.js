export default{
    todoItems: state => state.todoItems,
    searchItem: state => state.searchItem,
    deleteTodoItem: state => state.deleteTodoItem
}