import axios from "axios";
import router from "@/router";

export default {
  // Action for Register new User
  async registerNewUser(
    { commit },
    { email, password, password_confirmation }
  ) {
    commit("setIsLoading", true);
    try {
      const response = await axios.post("http://3.232.244.22/api/register", {
        email,
        password,
        password_confirmation,
      });
      if (response.data.success) {
        console.log(response.data.message);
        router.push({ name: "Login" });
      }
      commit("setIsLoading", false);
    } catch (error) {
      console.log(error);
    } finally {
      commit("setIsLoading", false);
    }
  },
  // Action for Request for Login
  async login({ commit }, { email, password }) {
    commit("setIsLoading", true);
    try {
      const response = await axios.post("http://3.232.244.22/api/login", {
        email,
        password,
      });
      if (response.data.success) {
        const authToken = response.data.user.token;
        commit("setToken", authToken);
        // Session
        sessionStorage.setItem("authToken", authToken);
        commit("setIsLoggedIn", true);
        router.push({ name: "ToDoList" });
      }
      commit("setIsLoading", false);
    } catch (error) {
      console.log(error);
      alert("Something went wong. please try again.");
    } finally {
      commit("setIsLoading", false);
    }
  },

  // App onload Check Login
  authentication({ commit }) {
    const authToken = sessionStorage.getItem("authToken");
    if (authToken) {
      commit("setIsLoggedIn", true);
    }
  },
  // Logout
  async logout({ commit }) {
    commit("setIsLoading", true);
    const authToken = sessionStorage.getItem("authToken");
    try {
      const response = await axios.post("http://3.232.244.22/api/logout", {
        token: authToken,
      });
      if (response.data.success) {
        sessionStorage.removeItem("authToken");
        commit("setToken", "");
        commit("setIsLoggedIn", false);
        router.push({ name: "Home" });
      }
      commit("setIsLoading", false);
    } catch (error) {
      console.log(error);
    } finally {
      commit("setIsLoading", false);
    }
  },
};
