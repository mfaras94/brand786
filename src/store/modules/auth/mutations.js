// mutations.js
export default {
  setIsLoggedIn(state, value) {
    state.isLoggedIn = value
  },
  setIsLoading(state,value){
    state.isLoading = value
  },
  setToken(state,value){
    state.token = value
  }

  }
  