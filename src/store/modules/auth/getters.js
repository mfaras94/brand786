// getters.js
export default {
   isLoggedIn: state => state.isLoggedIn,
   isLoading: state => state.isLoading,
   token: state => state.token
  }
  